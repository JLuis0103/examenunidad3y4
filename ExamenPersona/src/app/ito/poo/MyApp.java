package app.ito.poo;
import clases.ito.poo.Persona;

public class MyApp {
	
	static void run() throws CloneNotSupportedException {
		Persona p1 = new Persona ("Jose Luis", 271160058);
		Persona p2 = p1;
		System.out.println(p1);
		
		System.out.println("Clonando persona");
		Persona p3 = (Persona) p1.clone();
		
		System.out.println("Modificando nombre de persona 2");
		p2.setNombre("Luis Jose");
		
		System.out.println("Persona 1: " + p1 + "\nPersona 2: " + p2 + "\nPersona 3: " + p3);
		
	}

	public static void main(String[] args) throws CloneNotSupportedException {
		run();
	}

}
