package clases.ito.poo;

public class Persona implements Clonable{
	
	private String nombre = "";
	private long telefono = 0;
	
	public Persona() {
		super();
	}

	public Persona(String nombre, long telefono) {
		super();
		this.nombre = nombre;
		this.telefono = telefono;
	}
	
	//===========================================
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getTelefono() {
		return telefono;
	}

	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}
	
	//===========================================
	
	@Override
	public String toString() {
		return "Usuario: " + nombre + ", Telefono: " + telefono;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		Persona nueva = new Persona (this.nombre, this.telefono);
		return nueva;
	}
	
}
