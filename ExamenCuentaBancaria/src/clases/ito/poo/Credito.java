package clases.ito.poo;

public class Credito extends Cuenta{
	
	private float limiteCredito = 0F;
	private float creditoUtilizado = 0F;
	
	public Credito() {
		super();
	}
	
	public Credito(long numeroCuenta, String cliente, long telefono, float limiteCredito, float creditoUtilizado) {
		super(numeroCuenta, cliente, telefono);
		this.limiteCredito = limiteCredito;
		this.creditoUtilizado = creditoUtilizado;
	}
	
	//===========================================
	
	public void cargo (float cantidad) {
		if (cantidad <= limiteCredito - creditoUtilizado)
			creditoUtilizado = creditoUtilizado + cantidad;
	}
	
	public void pago (float cantidad) {
		if (cantidad <= creditoUtilizado && cantidad > 0)
			creditoUtilizado = creditoUtilizado - cantidad;
	}

	//===========================================
	
	@Override
	public String toString() {
		return "Cuenta de Credito" + "\nClave: " + getNumeroCuenta() + ", Cliente: "  + getCliente() + ", Telefono: "
				+ getTelefono() + "\nCredito Actual: " + limiteCredito + "\nCredito Utilizado: " + creditoUtilizado;
	}

}
