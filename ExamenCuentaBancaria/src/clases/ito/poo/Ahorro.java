package clases.ito.poo;
import java.time.LocalDate;

public class Ahorro extends Cuenta{
	
	private float saldo = 0F;
	private LocalDate ultimaActualizacion = null;
	
	public Ahorro() {
		super();
	}
	
	public Ahorro(long numeroCuenta, String cliente, long telefono, float saldo, LocalDate ultimaActualizacion) {
		super(numeroCuenta, cliente, telefono);
		this.saldo = saldo;
		this.ultimaActualizacion = ultimaActualizacion;
	}
	
	//===========================================
	
	public void deposito(float monto) {
		this.saldo = this.saldo + monto;
		this.ultimaActualizacion = LocalDate.now();
	}
	
	public void retiro(float cantidad) {
		if (cantidad <= this.saldo && cantidad > 0) {
			this.saldo=this.saldo-cantidad;
			this.ultimaActualizacion=LocalDate.now();
		}
	}

	//===========================================
	
	@Override
	public String toString() {
		return "Cuenta de Ahorro" + "\nClave: " + getNumeroCuenta() + ", Cliente: "  + getCliente() + ", Telefono: "
		+ getTelefono() + "\nSaldo actual: " + saldo + "\nUltima Actualizacion: " + ultimaActualizacion;
	}
	
}
