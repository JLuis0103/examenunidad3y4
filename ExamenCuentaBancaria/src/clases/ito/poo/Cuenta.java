package clases.ito.poo;

public class Cuenta {
	
	private long numeroCuenta = 0;
	private String cliente = "";
	private long telefono = 0;
	
	public Cuenta() {
		super();
	}

	public Cuenta(long numeroCuenta, String cliente, long telefono) {
		super();
		this.numeroCuenta = numeroCuenta;
		this.cliente = cliente;
		this.telefono = telefono;
	}
	
	//===========================================

	public long getNumeroCuenta() {
		return numeroCuenta;
	}

	public void setNumeroCuenta(long numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public long getTelefono() {
		return telefono;
	}

	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}
	
	//===========================================

	@Override
	public String toString() {
		return "Numero de cuenta: " + numeroCuenta + "/nCliente: " + cliente + "/nTelefono: " + telefono;
	}
	
}
