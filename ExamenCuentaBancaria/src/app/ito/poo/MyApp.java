package app.ito.poo;
import clases.ito.poo.Ahorro;
import clases.ito.poo.Credito;
import java.time.LocalDate;

public class MyApp {
	
	static void run() {
		Ahorro a1 = new Ahorro (1234567890, "Jose Luis Ramirez", 271160058, 20000, LocalDate.now());
		System.out.println(a1);
		a1.deposito(1500);
		System.out.println(a1);
		a1.retiro(250000);
		System.out.println(a1);
		a1.retiro(1500);
		System.out.println(a1);
		
		System.out.println();
		
		Credito c1 = new Credito(1234567890, "Jose Luis Ramirez", 271160058, 20000, 0);
		System.out.println(c1);
		c1.cargo(23585);
		System.out.println(c1);
		c1.cargo(5000);
		System.out.println(c1);
		c1.pago(6000);
		System.out.println(c1);
		c1.pago(2500);
		System.out.println(c1);
	}

	public static void main(String[] args) {
		run();
	}

}
